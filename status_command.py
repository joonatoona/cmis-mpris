#!/usr/bin/env python3

import dbus
import os

from cmus import CmusRemote

m = CmusRemote.get_playing()
if not os.path.exists(m.cover):
    m.update_cover()

bus = dbus.SessionBus()
obj = bus.get_object("org.mpris.MediaPlayer2.cmus", "/org/mpris/MediaPlayer2")
p_iface = dbus.Interface(obj, dbus.PROPERTIES_IFACE)

for v in ["PlaybackStatus", "Metadata", "CanGoNext", "CanGoPrevious", "CanPlay", "CanPause", "Position"]:
    p_iface.Set("org.mpris.MediaPlayer2.Player", v, 0)
